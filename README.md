# Fastxt
[fastxt.app](https://fastxt.app)

Fastxt, as a sister project to [Local Native](https://localnative.app), with very similar design and toolchain, is a decentralized cross-platform application to save and sync your txt in local SQLite database without going through any centralized service.

# Videos
[Local Native YouTube Channel](https://www.youtube.com/channel/UCO3qFIyK0eSmqvMknsslWRw)

## Articles
[Updates](https://fastxt.app/blog)


# Sub-directories

fastxt-android: Android App

fastxt-ios: iOS and iPadOS App

fastxt-mac: macOS App

fastxt-rs: rust code, as shared core library used by other presentation layers

fastxt-win: Windows App


# Developer Setup
[here](https://fastxt.app/developer-setup.html)

# License
[AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)

# Contribution
[Contributor Covenant Code of Conduct](https://www.contributor-covenant.org/version/1/4/code-of-conduct)

## Developer Certificate of Origin
Inspired by and borrowed from [Librem5](https://developer.puri.sm/Librem5/Appendix/dco.html).

The Developer Certificate of Origin (DCO) ensures that persons contributing code are allowed to do so and that the project stays under a free license. This is indicated by a sign-off.

The sign-off is a simple line at the end of the explanation for the patch, which certifies that you wrote it or otherwise have the right to pass it on as an open-source patch. The rules are pretty simple: if you can certify the below:

```
Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.
```

then you just add a line saying:

```
Signed-off-by: Developer Name <name@developer.example.org>
```

using your real name (sorry, no pseudonyms or anonymous contributions.)

## Screenshot

### Desktop Application
![Fastxt desktop application](https://fastxt.app/img/fastxt-sync-server-qr-mac.png)

## Support
<a href="https://www.patreon.com/localnative">
<img alt="Become a Patron!" src="https://c5.patreon.com/external/logo/become_a_patron_button.png" />
</a>
<a href="https://opencollective.com/localnative/donate" target="_blank">
  <img src="https://opencollective.com/localnative/donate/button@2x.png?color=blue" width=300 />
</a>
